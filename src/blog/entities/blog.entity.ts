import { ObjectType, Field, Int } from '@nestjs/graphql';

@ObjectType()
export class Blog {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;

  @Field(() => Int)
  id: number;

  createdAt: Date;

  @Field()
  blog_title: string;

  @Field()
  blog_content: string;

  @Field()
  authorId: number;

  @Field()
  like: number;
}
