import { ObjectType, Field, Int } from '@nestjs/graphql';

@ObjectType()
export class User {
  @Field((type) => Int)
  id: number;

  @Field()
  user_name: string;

  @Field()
  user_email: string;

  @Field()
  user_address: string;

  @Field()
  username: string;

  @Field()
  password: string;
}
