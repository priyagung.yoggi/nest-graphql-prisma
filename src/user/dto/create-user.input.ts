import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateUserInput {
  @Field()
  user_name: string;

  @Field()
  user_email: string;

  @Field()
  user_address: string;

  @Field()
  username: string;

  @Field()
  password: string;
}
