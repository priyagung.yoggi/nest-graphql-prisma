import { ConflictException, Injectable } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { PrismaClient, User } from '@prisma/client';
const prisma = new PrismaClient();
import * as bcrypt from 'bcryptjs';
@Injectable()
export class UserService {
  async create(createUserInput: CreateUserInput) {
    const hashPassword = await bcrypt.hash(createUserInput.password, 10);
    const { user_email, user_address, user_name, username } = createUserInput;
    try {
      const user = await prisma.user.create({
        data: {
          user_address: user_address,
          user_email: user_email,
          user_name: user_name,
          username: username,
          password: hashPassword,
        },
      });
      return user;
    } catch (error) {
      throw new ConflictException('email already exist');
    }
  }

  async findAll(): Promise<User[]> {
    return prisma.user.findMany({});
  }

  async findOne(email: string) {
    return prisma.user.findUnique({
      where: {
        user_email: email,
      },
    });
  }

  update(id: number, updateUserInput: UpdateUserInput) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
