export declare class CreateUserInput {
    user_name: string;
    user_email: string;
    user_address: string;
    username: string;
    password: string;
}
