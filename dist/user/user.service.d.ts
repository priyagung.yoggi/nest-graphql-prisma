import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from '@prisma/client';
export declare class UserService {
    create(createUserInput: CreateUserInput): Promise<User>;
    findAll(): Promise<User[]>;
    findOne(email: string): Promise<User>;
    update(id: number, updateUserInput: UpdateUserInput): string;
    remove(id: number): string;
}
