"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient();
const bcrypt = require("bcryptjs");
let UserService = class UserService {
    async create(createUserInput) {
        const hashPassword = await bcrypt.hash(createUserInput.password, 10);
        const { user_email, user_address, user_name, username } = createUserInput;
        try {
            const user = await prisma.user.create({
                data: {
                    user_address: user_address,
                    user_email: user_email,
                    user_name: user_name,
                    username: username,
                    password: hashPassword,
                },
            });
            return user;
        }
        catch (error) {
            throw new common_1.ConflictException('email already exist');
        }
    }
    async findAll() {
        return prisma.user.findMany({});
    }
    async findOne(email) {
        return prisma.user.findUnique({
            where: {
                user_email: email,
            },
        });
    }
    update(id, updateUserInput) {
        return `This action updates a #${id} user`;
    }
    remove(id) {
        return `This action removes a #${id} user`;
    }
};
UserService = __decorate([
    common_1.Injectable()
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map