export declare class Blog {
    exampleField: number;
    id: number;
    createdAt: Date;
    blog_title: string;
    blog_content: string;
    authorId: number;
    like: number;
}
