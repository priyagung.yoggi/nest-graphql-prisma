import { CreateBlogInput } from './create-blog.input';
declare const UpdateBlogInput_base: import("@nestjs/common").Type<Partial<CreateBlogInput>>;
export declare class UpdateBlogInput extends UpdateBlogInput_base {
    id: number;
}
export {};
