import { BlogService } from './blog.service';
import { CreateBlogInput } from './dto/create-blog.input';
import { UpdateBlogInput } from './dto/update-blog.input';
export declare class BlogResolver {
    private readonly blogService;
    constructor(blogService: BlogService);
    createBlog(createBlogInput: CreateBlogInput): string;
    findAll(): string;
    findOne(id: number): string;
    updateBlog(updateBlogInput: UpdateBlogInput): string;
    removeBlog(id: number): string;
}
